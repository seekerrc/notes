# SSM框架整合

前置需求：安装JDK8、IDEA、MySQL5.7、SQLyog、Maven3.6、Tomcat 9

环境配置自行百度，如果前面的课程都有跟着配置，那么，这些需求自然满足

## Mybatis层

### 数据库创建

1、启动sql服务。

注意点：管理员身份打开cmd

```cmd
net start mysql
```

2、利用可视化界面连接数据库

使用sql语句创建表格

```sql
CREATE DATABASE `ssmbuild`;

USE `ssmbuild`;

DROP TABLE IF EXISTS `books`;

CREATE TABLE `books` (
`bookID` INT(10) NOT NULL AUTO_INCREMENT COMMENT '书id',
`bookName` VARCHAR(100) NOT NULL COMMENT '书名',
`bookCounts` INT(11) NOT NULL COMMENT '数量',
`detail` VARCHAR(200) NOT NULL COMMENT '描述',
KEY `bookID` (`bookID`)
) ENGINE=INNODB DEFAULT CHARSET=utf8

INSERT INTO `books`(`bookID`,`bookName`,`bookCounts`,`detail`)VALUES
(1,'Java',1,'从入门到放弃'),
(2,'MySQL',10,'从删库到跑路'),
(3,'Linux',5,'从进门到进牢');
```

注意点：右键刷新左侧的数据库条目

![image-20210426194803022](SSM%E6%A1%86%E6%9E%B6%E6%95%B4%E5%90%88.assets/image-20210426194803022.png)

### 项目创建

1、创建一个普通的maven项目，一路next

然后进入项目的pom.xml配置文件，进行环境配置

注意点：出现弹窗必须选择auto-import

![image-20210426195625213](SSM%E6%A1%86%E6%9E%B6%E6%95%B4%E5%90%88.assets/image-20210426195625213.png)

2、配置pom.xml文件

第一步、依赖项

```xml
<dependencies>
    <!--Junit-->
    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.12</version>
    </dependency>
    <!--数据库驱动-->
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <version>5.1.47</version>
    </dependency>
    <!-- 数据库连接池 -->
    <dependency>
        <groupId>com.mchange</groupId>
        <artifactId>c3p0</artifactId>
        <version>0.9.5.2</version>
    </dependency>
    <!--Servlet - JSP -->
    <dependency>
        <groupId>javax.servlet</groupId>
        <artifactId>servlet-api</artifactId>
        <version>2.5</version>
    </dependency>
    <dependency>
        <groupId>javax.servlet.jsp</groupId>
        <artifactId>jsp-api</artifactId>
        <version>2.2</version>
    </dependency>
    <dependency>
        <groupId>javax.servlet</groupId>
        <artifactId>jstl</artifactId>
        <version>1.2</version>
    </dependency>
    <!--Mybatis-->
    <dependency>
        <groupId>org.mybatis</groupId>
        <artifactId>mybatis</artifactId>
        <version>3.5.2</version>
    </dependency>
    <dependency>
        <groupId>org.mybatis</groupId>
        <artifactId>mybatis-spring</artifactId>
        <version>2.0.2</version>
    </dependency>
    <!--Spring-->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-webmvc</artifactId>
        <version>5.1.9.RELEASE</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-jdbc</artifactId>
        <version>5.1.9.RELEASE</version>
    </dependency>
</dependencies>
```

第二步、静态资源导出问题，确保项目额外的配置文件可以被读取到

```xml
<build>
    <resources>
        <resource>
            <directory>src/main/java</directory>
            <includes>
                <include>**/*.properties</include>
                <include>**/*.xml</include>
            </includes>
            <filtering>false</filtering>
        </resource>
        <resource>
            <directory>src/main/resources</directory>
            <includes>
                <include>**/*.properties</include>
                <include>**/*.xml</include>
            </includes>
            <filtering>false</filtering>
        </resource>
    </resources>
</build>
```

### 链接数据库

1、添加mysql链接

<img src="SSM%E6%A1%86%E6%9E%B6%E6%95%B4%E5%90%88.assets/image-20210426202940863.png" alt="image-20210426202940863" style="zoom:50%;" />

2、输入root和123456，测试链接，如果没有驱动，点击下方的下载即可

3、schemas选项栏，选择ssmbuild数据库，点击ok就可以查看表的内容

![image-20210426203207912](SSM%E6%A1%86%E6%9E%B6%E6%95%B4%E5%90%88.assets/image-20210426203207912.png)

### 项目结构创建

1、创建包和配置文件，结果如图

<img src="SSM%E6%A1%86%E6%9E%B6%E6%95%B4%E5%90%88.assets/image-20210426203804675.png" alt="image-20210426203804675" style="zoom:50%;" />

2、配置文件基本内容

mybatis-config

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
</configuration>
```

applicationContext

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/springbeans.xsd">
</beans>
```

出现提示的时候选中create就好了，这样就会被加入到spring的配置当中

左侧行号附近会出现小叶子的标记

![image-20210426204031066](SSM%E6%A1%86%E6%9E%B6%E6%95%B4%E5%90%88.assets/image-20210426204031066.png)

### Mybatis数据层代码书写

1、在resources目录下创建数据库链接配置文件database.properties

```properties
jdbc.driver=com.mysql.jdbc.Driver
jdbc.url=jdbc:mysql://localhost:3306/ssmbuild?useSSL=true&useUnicode=true&characterEncoding=utf8
jdbc.username=root
jdbc.password=123456
```

2、配置mybatis配置文件

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
    <typeAliases>
        <package name="com.kuang.pojo"/>
    </typeAliases>
</configuration>
```

3、写实体类

推荐使用lombok，安装参考

https://blog.csdn.net/qq_36761831/article/details/86546970?utm_medium=distribute.pc_relevant_t0.none-task-blog-2~default~BlogCommendFromBaidu~default-1.control

在项目pom中配置依赖，直接复制，快捷导入可能会失灵

```xml
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <version>1.16.10</version>
</dependency>
```

在pojo下新建class文件

```java
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Books {

    private int bookID;
    private String bookName;
    private int bookCounts;
    private String detail;

}
```

5、创建dao的接口

```java
public interface BookMapper {

    int addBook(Books books);

    int deleteBookById(int id);

    int updateBook(Books books);

    Books queryBookById(int id);

    List<Books> queryAllBook();

}
```

6、编写mapper的xml，这里是最底层的SQL语句

推荐分屏书写代码

![image-20210426213617150](SSM%E6%A1%86%E6%9E%B6%E6%95%B4%E5%90%88.assets/image-20210426213617150.png)

快捷书写：先打<然后根据提示回车，打>时IDEA会自动生成闭合标签，比输入完整的tag然后tab更人性化

参数快捷书写：直接输入首字母，根据提示回车

SQL语句快速书写：

ins：自动生成INSERT语句模板；

upd：UPDATE模板

sel：SELECT * FROM

其他的语句有些是没有的，可以自己设置一下

就算是内容IDEA也能提示

![image-20210426214254286](SSM%E6%A1%86%E6%9E%B6%E6%95%B4%E5%90%88.assets/image-20210426214254286.png)

书写后：

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.kuang.dao.BookMapper">

    <insert id="addBook" parameterType="Books">
        insert into ssmbuild.books (bookName, bookCounts, detail)
        values (#{bookName},#{bookCounts},#{detail});
    </insert>

    <delete id="deleteBookById" parameterType="int">
        delete from ssmbuild.books where bookID = #{bookId}
    </delete>

    <update id="updateBook" parameterType="Books">
        update ssmbuild.books
        set bookName=#{bookName},
        bookCounts=#{bookCounts}, detail=#{detail}
        where bookID=#{bookID};
    </update>

    <select id="queryBookById" resultType="Books">
        select * from ssmbuild.books
        where bookID=#{bookId};
    </select>

    <select id="queryAllBook" resultType="Books">
        select * from ssmbuild.books;
    </select>

</mapper>
```

7、写完mapper的xml配置文件的第一件事：前往mybatis-config中声明映射

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
    <typeAliases>
        <package name="com.kuang.pojo"/>
    </typeAliases>
    <mappers>
        <mapper class="com.kuang.dao.BookMapper"/>
    </mappers>
</configuration>
```

自闭合标签的快速写法：输入/，直接就会生成/>，不需要额外的配置

8、业务层接口

和DAO层一样，只不过需要删除@param，因为业务层不需要

```java
public interface BookService {
    
    int addBook(Books books);

    int deleteBookById(int id);

    int updateBook(Books books);

    Books queryBookById(int id);

    List<Books> queryAllBook();
}

```

9、编写业务层实现类

一般在原始命名后面加上Impl后缀

快速编写：implements之后，使用alt+enter快捷键自动修复，选择实现方法就可以自动生成重写的方法结构



记住：业务层调用DAO层，只是横切进去了一些额外的业务功能

面向对象的思想：要调用DAO层，就要组合进来，意思就是要把DAO层的对象放到业务层做成员

在实现类中加入如下代码

自动生成set的快捷键：<kbd>alt</kbd> + <kbd>shift</kbd> + <kbd>s</kbd> 

```java
private BookMapper bookMapper;

public void setBookMapper(BookMapper bookMapper) {
    this.bookMapper = bookMapper;
}
```

从而有业务层的实现类

```java
public class BookServiceImpl implements BookService {

    private BookMapper bookMapper;

    public void setBookMapper(BookMapper bookMapper) {
        this.bookMapper = bookMapper;
    }

    public int addBook(Books books) {
        return bookMapper.addBook(books);
    }

    public int deleteBookById(int id) {
        return bookMapper.deleteBookById(id);
    }

    public int updateBook(Books books) {
        return bookMapper.updateBook(books);
    }

    public Books queryBookById(int id) {
        return bookMapper.queryBookById(id);
    }

    public List<Books> queryAllBook() {
        return bookMapper.queryAllBook();
    }
}
```

到此为止数据层就书写完毕了，看一看项目结构：

![image-20210426223454280](SSM%E6%A1%86%E6%9E%B6%E6%95%B4%E5%90%88.assets/image-20210426223454280.png)

## Spring层

### 整合DAO

spring-dao.xml

弹出提示时选择添加到ApplicationContext中就行

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/context https://www.springframework.org/schema/context/spring-context.xsd">

    <!--整合Mybatis-->

    <!--1、关联数据库配置-->
    <context:property-placeholder location="classpath:database.properties"/>

    <!-- 2、数据库连接池 -->
    <!--数据库连接池
    dbcp 半自动化操作 不能自动连接
    c3p0 自动化操作（自动的加载配置文件 并且设置到对象里面）
    -->
    <bean id="dataSource"
          class="com.mchange.v2.c3p0.ComboPooledDataSource">
        <!-- 配置连接池属性 -->
        <property name="driverClass" value="${jdbc.driver}"/>
        <property name="jdbcUrl" value="${jdbc.url}"/>
        <property name="user" value="${jdbc.username}"/>
        <property name="password" value="${jdbc.password}"/>
        <!-- c3p0连接池的私有属性 -->
        <property name="maxPoolSize" value="30"/>
        <property name="minPoolSize" value="10"/>
        <!-- 关闭连接后不自动commit -->
        <property name="autoCommitOnClose" value="false"/>
        <!-- 获取连接超时时间 -->
        <property name="checkoutTimeout" value="10000"/>
        <!-- 当获取连接失败重试次数 -->
        <property name="acquireRetryAttempts" value="2"/>
    </bean>

    <!--3、配置SqlSessionFactory对象-->

    <bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
        <!-- 注入数据库连接池 -->
        <property name="dataSource" ref="dataSource"/>
        <!-- 配置MyBatis全局配置文件:mybatis-config.xml -->
        <property name="configLocation" value="classpath:mybatis-config.xml"/>

    </bean>

    <!-- 4.配置扫描Dao接口包，动态实现Dao接口注入到spring容器中 -->
    <!--解释 ： https://www.cnblogs.com/jpfss/p/7799806.html-->
    <bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
        <!-- 注入sqlSessionFactory -->
        <property name="sqlSessionFactoryBeanName"
                  value="sqlSessionFactory"/>
        <!-- 给出需要扫描Dao接口包 -->
        <property name="basePackage" value="com.kuang.dao"/>
    </bean>
    
</beans>
```

### 整合Service

spring-service.xml

弹出提示时选择添加到ApplicationContext中就行

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/context
       https://www.springframework.org/schema/context/spring-context.xsd">

    <!-- 扫描service相关的bean -->
    <context:component-scan base-package="com.kuang.service" />

    <!--BookServiceImpl注入到IOC容器中-->
    <bean id="BookServiceImpl"
          class="com.kuang.service.BookServiceImpl">
        <property name="bookMapper" ref="bookMapper"/>
    </bean>

    <!-- 配置事务管理器 -->
    <bean id="transactionManager"
          class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
        <!-- 注入数据库连接池 -->
        <property name="dataSource" ref="dataSource" />
    </bean>

    <!--其他AOP事务支持-->

</beans>
```

为确保配置文件可以互相引用，可以在总的xml文件中进行import

applicationContext.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd">

    <import resource="classpath:spring-dao.xml"/>

    <import resource="classpath:spring-service.xml"/>

</beans>
```

## SpringMVC层

### 配置

1、前置条件：更新IDEA版本，只有最新的IDE版本才支持web4.0版本

![image-20210428133448840](SSM%E6%A1%86%E6%9E%B6%E6%95%B4%E5%90%88.assets/image-20210428133448840.png)

2、添加web支持

<img src="SSM%E6%A1%86%E6%9E%B6%E6%95%B4%E5%90%88.assets/image-20210428133628368.png" alt="image-20210428133628368" style="zoom:50%;" />

3、如果没有生成web.xml，选择项目结构，手动添加

参考链接：https://blog.csdn.net/u010758410/article/details/78780144

4、配置web.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">

    <!--DispatcherServlet-->
    <servlet>
        <servlet-name>DispatcherServlet</servlet-name>
        <servlet-class>
            org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <init-param>
            <param-name>contextConfigLocation</param-name>
            <!--一定要注意:我们这里加载的是总的配置文件，之前被这里坑了！-->
            <param-value>classpath:applicationContext.xml</param-value>
        </init-param>
        <load-on-startup>1</load-on-startup>
    </servlet>
    <servlet-mapping>
        <servlet-name>DispatcherServlet</servlet-name>
        <url-pattern>/</url-pattern>
    </servlet-mapping>

    <!--encodingFilter-->
    <filter>
        <filter-name>encodingFilter</filter-name>
        <filter-class>
            org.springframework.web.filter.CharacterEncodingFilter
        </filter-class>
        <init-param>
            <param-name>encoding</param-name>
            <param-value>utf-8</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>encodingFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>

    <!--Session过期时间-->
    <session-config>
        <session-timeout>15</session-timeout>
    </session-config>

</web-app>
```

5、配置spring-mvc.xml

前面这一大串都不要手动敲，<mvc:和<context:的时候 <kbd>alt</kbd> + <kbd>enter</kbd> 就完事了

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/mvc
       https://www.springframework.org/schema/mvc/spring-mvc.xsd
       http://www.springframework.org/schema/context
       https://www.springframework.org/schema/context/spring-context.xsd">

    <!-- 配置SpringMVC -->
    <!-- 1.开启SpringMVC注解驱动 -->
    <mvc:annotation-driven />

    <!-- 2.静态资源默认servlet配置-->
    <mvc:default-servlet-handler/>

    <!-- 3.配置jsp 显示ViewResolver视图解析器 -->
    <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <property name="viewClass"
                  value="org.springframework.web.servlet.view.JstlView" />
        <property name="prefix" value="/WEB-INF/jsp/" />
        <property name="suffix" value=".jsp" />
    </bean>

    <!-- 4.扫描web相关的bean -->
    <context:component-scan base-package="com.kuang.controller" />

</beans>
```

6、记得在总的配置文件applicationContext.xml中添加

```xml
<import resource="classpath:spring-mvc.xml"/>
```

7、对应创建文件夹

<img src="SSM%E6%A1%86%E6%9E%B6%E6%95%B4%E5%90%88.assets/image-20210428142357715.png" alt="image-20210428142357715" style="zoom:50%;" />

### 功能代码

#### 查询

1、在controller包中新建BookController类

```java
@Controller
@RequestMapping("/book")
public class BookController {

    //controller层调用的是service层，自动装配，IOC
    @Autowired
    @Qualifier("BookServiceImpl")
    private BookService bookService;

    //查询全部书籍，返回一个展示书籍的页面
    @RequestMapping("/allBook")
    public String list(Model model){
        List<Books> list = bookService.queryAllBook();
        model.addAttribute("list", list);
        return "allBook";//默认转发，视图解析器进行路径拼接，重定向需要明确指出redirect
    }
  
}
```

2、编写主页面和展示书籍的页面

格式化代码：<kbd>Ctrl</kbd> + <kbd>alt</kbd> + <kbd>L</kbd> 

首页index.jsp

```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>首页</title>
    <style type="text/css">
        a {
            text-decoration: none;
            color: black;
            font-size: 18px;
        }
        h3 {
            width: 180px;
            height: 38px;
            margin: 100px auto;
            text-align: center;
            line-height: 38px;
            background: deepskyblue;
            border-radius: 4px;
        }
    </style>
</head>
<body>
<h1>
    <a href="${pageContext.request.contextPath}/book/allBook">进入书籍页面</a>
</h1>
</body>
</html>
```

展示书籍allBook.jsp

```jsp
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>书籍列表</title>
    <meta name="viewport" content="width=device-width, initialscale=1.0">
    <!-- 引入 Bootstrap -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <div class="page-header">
                <h1>
                    <small>书籍列表 —— 显示所有书籍</small>
                </h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 column">
            <a class="btn btn-primary" href="${pageContext.request.contextPath}/book/toAddBook">新增</a>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-md-12 column">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th>书籍编号</th>
                    <th>书籍名字</th>
                    <th>书籍数量</th>
                    <th>书籍详情</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="book" items="${requestScope.get('list')}">
                    <tr>
                        <td>${book.getBookID()}</td>
                        <td>${book.getBookName()}</td>
                        <td>${book.getBookCounts()}</td>
                        <td>${book.getDetail()}</td>
                        <td>
                            <a href="${pageContext.request.contextPath}/book/toUpdateBook?id=${book.getBookID()}">更改</a> |
                            <a href="${pageContext.request.contextPath}/book/del/${book.getBookID()}">删除</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
```



3、配置tomcat9 

第二步选择local server

![image-20210428145836002](SSM%E6%A1%86%E6%9E%B6%E6%95%B4%E5%90%88.assets/image-20210428145836002.png)

4、配置Artifacts，注意检查lib目录是否添加，否则手动加上去

![image-20210428151444915](SSM%E6%A1%86%E6%9E%B6%E6%95%B4%E5%90%88.assets/image-20210428151444915.png)

5、单击运行tomcat，会自动弹出浏览器窗口



调试时直接点击运行，根据需求选择重新运行的方式就可以了

- update classes and resources：改前端代码，jsp
- redeploy：改java代码
- restart server：改xml配置文件

![image-20210428152423480](SSM%E6%A1%86%E6%9E%B6%E6%95%B4%E5%90%88.assets/image-20210428152423480.png)

#### 添加

A。首先，在主界面添上增加书籍的按钮

```jsp
<div class="row">
    <div class="col-md-4 column">
        <a class="btn btn-primary" href="${pageContext.request.contextPath}/book/toAddBook">新增</a>
    </div>
</div>
```

B。添加书籍需要一个新的页面，addBook.jsp

```jsp
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>新增书籍</title>
    <meta name="viewport" content="width=device-width, initialscale=1.0">
    <!-- 引入 Bootstrap -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <div class="page-header">
                <h1>
                    <small>新增书籍</small>
                </h1>
            </div>
        </div>
    </div>
    <form action="${pageContext.request.contextPath}/book/addBook" method="post">
        <div class="form-group">
            <label>书籍名称</label>
            <input type="text" name="bookName" class="form-control" required>
        </div>
        <div class="form-group">
            <label>书籍数量</label>
            <input type="text" name="bookCounts" class="form-control" required>
        </div>
        <div class="form-group">
            <label>书籍详情</label>
            <input type="text" name="detail" class="form-control" required>
        </div>
        <div class="form-group">
            <input type="submit" value="添加" class="form-control">
        </div>

    </form>
</div>
```

controller层代码添加

```java
@RequestMapping("/toAddBook")          //C1
public String toAddPaper(){
    return "addBook";
}

@RequestMapping("/addBook")			   //C2
public String addPaper(Books books){
    System.out.println(books);
    bookService.addBook(books);
    return "redirect:/book/allBook/";
}
```

注意理解这里的逻辑：

首先，我们需要实现从按钮点击到新页面加载的跳转，这里通过/toAddBook地址实现，它做的其实就是请求转发

然后，我们在新的addBook.jsp页面填写表单，填完提交之后需要跳转回到主页面显示添加后的书籍列表，这里是通过/addBook地址来实现的，它完成添加书籍的业务（bookService）之后，会做一个重定向到主页面

所以执行顺序是：A点击按钮，C1实现请求转发页面显示，B新页面添加信息，C2捕捉B提交的表单实现业务逻辑后重定向，回到A主界面

#### 修改

println之类的调试信息点击run查看

![image-20210429171544694](SSM%E6%A1%86%E6%9E%B6%E6%95%B4%E5%90%88.assets/image-20210429171544694.png)

或者打印数据库日志调试，修改mybatis-config.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
    <settings>
        <setting name="logImpl" value="STDOUT_LOGGING"/>
    </settings>
    <typeAliases>
        <package name="com.kuang.pojo"/>
    </typeAliases>
    <mappers>
        <mapper class="com.kuang.dao.BookMapper"/>
    </mappers>
</configuration>
```



1、主界面添加链接

```jsp
<a href="${pageContext.request.contextPath}/book/toUpdate?id=${book.getBookID()}">更改</a>
```

2、书写toUpdate链接下的java代码

```java
@RequestMapping("/toUpdate")
public String toUpdatePaper(Model model, int id){
    Books books = bookService.queryBookById(id);
    model.addAttribute("Qbook", books);
    return "updateBook";
}
```

就是获取信息和请求转发

3、书写updateBook.jsp页面

表单必须提交ID才能完成数据库的修改，这里设置input为hidden，不然不知道修改的是哪一个对象

```jsp
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>新增书籍</title>
    <meta name="viewport" content="width=device-width, initialscale=1.0">
    <!-- 引入 Bootstrap -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <div class="page-header">
                <h1>
                    <small>修改书籍</small>
                </h1>
            </div>
        </div>
    </div>
    <form action="${pageContext.request.contextPath}/book/updateBook" method="post">
        <input type="hidden" name="bookID" value="${Qbook.getBookID()}"/>
        <div class="form-group">
            <label>书籍名称</label>
            <input type="text" name="bookName" class="form-control" value="${Qbook.bookName}" required>
        </div>
        <div class="form-group">
            <label>书籍数量</label>
            <input type="text" name="bookCounts" class="form-control" value="${Qbook.bookCounts}" required>
        </div>
        <div class="form-group">
            <label>书籍详情</label>
            <input type="text" name="detail" class="form-control" value="${Qbook.detail}" required>
        </div>
        <div class="form-group">
            <input type="submit" value="修改" class="form-control">
        </div>

    </form>
</div>
```

4、书写updateBook链接下的java代码

```java
@RequestMapping("/updateBook")
public String updateBook(Books books){
    System.out.println("updateBook======="+books);//注意查看信息，有助于调试
    bookService.updateBook(books);
    return "redirect:/book/allBook/";
}
```

#### 删除

1、主页面链接

注意这里是RESTful风格的url

```jsp
<a href="${pageContext.request.contextPath}/book/del/${book.getBookID()}">删除</a>
```

2、不需要额外的页面，直接删就好了，删了之后重定向显示所有书籍

```java
@RequestMapping("/del/{bookId}")
public String deleteBook(@PathVariable("bookId") int id){
    bookService.deleteBookById(id);
    return "redirect:/book/allBook/";
}
```

#### 额外的功能

以补充额外的查询功能为例：

1. 首先修改前端jsp，增加查询框和按钮
2. 编写查询的controller，有个大致的框架，需要什么参数，页面跳转
3. 从底层写起，先在mapper接口中添加一个查询的函数
4. 再对应编写mapper.xml中的SQL语句
5. service层在接口和实现类中添加上述函数，调用dao层进行实现
6. controller层调用service层，并且完善信息的传递

以上就是添加一个基本功能在SSM框架当中的编写流程了

后续的优化：

1. 增加一些条件语句，捕捉特殊情况（比如没有查到），并且将信息输出到前端页面
2. 添加一些固定功能（显示所有书籍）的按钮，提升用户体验

