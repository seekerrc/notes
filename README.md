# Notes

## 介绍
专门用来存学习笔记的地方

B站专栏：https://www.bilibili.com/read/cv11085927

记得下载同名的图片文件夹，笔记整理不易，如果对你有帮助，不妨点个赞支持一下，谢谢啦~



## 文件
### SSM框架整合

视频教程（狂神说）：https://www.bilibili.com/video/BV1RE41127rv

食用方法：跟着做

笔记目录结构：

![SSM框架整合文件目录](README.assets/18.png)



### 设计模式

视频教程（尚硅谷）：https://www.bilibili.com/video/BV1G4411c7N4

食用方法：听完课看一遍，笔记很浓缩的，可以根据自己的理解补充这份笔记（fork）

笔记目录结构：

![设计模式](README.assets/19.png)





### SpringCloud2020

视频教程尚硅谷：https://www.bilibili.com/video/BV18E411x7eT?p=1

为了看个笔记买MindManager难以接受，我把mmap文件转换为了html文件，大家下载SpringCloud2020.html之后右键用浏览器打开就可以直接看，笔记没毛病，相当丝滑，代码和图片都无损显示，如图所示



![image-20211105202054516](README.assets/image-20211105202054516.png)







